from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError

from .models import Player


class PlayerForm(UserCreationForm):
	class Meta:
		model = Player
		fields = ('first_name', 'last_name', 'username', 'password1', 'password2', 'email', )


class GoalForm(forms.ModelForm):
	goal = forms.IntegerField(label='Your goal', min_value=1, max_value=200)
	actual_record = forms.IntegerField(label='Your actual record', min_value=0, max_value=200)

	def clean(self):
		# check if user set higher goal than actual record
		cleaned_data = super(GoalForm, self).clean()

		actual_record = cleaned_data.get('actual_record')
		goal = cleaned_data.get('goal')
		if goal <= actual_record:
			raise ValidationError('Your record cannot be equal or lower than goal', code='invalid')

	class Meta:
		model = Player
		fields = ('goal', 'actual_record', 'id')
