from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.core.exceptions import FieldDoesNotExist
from django.db import models
from django.utils import timezone


class Player(User):
	goal = models.IntegerField(blank=True, null=True)
	actual_record = models.IntegerField(blank=True, null=True)

	@property
	def reps_done(self):
		total = 0
		trainings = self.trainings.filter(is_done=True)
		for training in trainings:
			total += training.reps_done
		return total

	@property
	def sessions_done(self):
		sessions = self.trainings.filter(is_done=True).order_by('-done_date')
		return sessions

	@property
	def sessions_done_number(self):
		sessions = self.trainings.filter(is_done=True)
		return len(sessions)

	def get_training_session(self):
		''' Function returning proper training session for player
		:return: TrainingSession object
		'''
		if not self.goal:
			return None
		undone = self.trainings.filter(is_done=False).first()
		if undone:
			return undone
		session = TrainingSession.create(user=self)
		return session


class TrainingSession(models.Model):
	is_done = models.BooleanField(default=False)
	user = models.ForeignKey(Player, related_name='trainings')
	series = models.TextField()
	reps_done = models.PositiveIntegerField(default=0)
	created_date = models.DateTimeField(default=timezone.now)
	done_date = models.DateTimeField(blank=True, null=True)
	completed = models.BooleanField(default=False)
	ratio = models.FloatField(default=1.0)

	@staticmethod
	def generate_series(goal, number_of_series, ratio):
		''' Generate new training series
		:param ratio: float number between 0 and 1 describing level of training plan
		:param number_of_series: integer
		:return: string with numbers splitted by spaces
		'''
		goal = int(ratio * goal)
		serie = int(goal / number_of_series)
		if serie < 1:
			serie = 1
		series = [serie] * number_of_series

		# add difference from rounding
		difference = goal - (serie * number_of_series)
		if difference > 0:
			series[0] += difference
		# return ' '.join(map(str, series))
		return series

	@classmethod
	def create(cls, user):
		''' Generate new session based on history
		'''
		# last_users_training = TrainingSession.objects.filter(user=self.user).order_by('-done_date').first()
		last_users_training = user.trainings.order_by('-done_date').first()

		# check if last training was successfully completed, otherwise make next training easier
		if last_users_training and not last_users_training.completed:
			ratio = 0.5
		else:
			ratio = 1.0
		try:
			series = cls.generate_series(goal=user.goal, number_of_series=4, ratio=ratio)
		except FieldDoesNotExist: #for admin debugging
			series = '10 10 10 10'
		training = cls(user=user, series=series, ratio=ratio)
		return training
