from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.dashboard, name='dashboard'),
	url(r'^training_session/', views.training_session, name='training_session'),
	url(r'^training_summary/', views.training_summary, name='training_summary'),
	url(r'^goal/$', views.GoalView.as_view(), name='apply_goal'),
	url(r'^register_player/', views.PlayerView.as_view(), name='register_player'),
	url(r'^chart/', views.trainings_chart, name='chart'),
]
