import datetime
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic.edit import FormView, UpdateView

from .forms import GoalForm, PlayerForm
from .models import TrainingSession, Player


@login_required
def dashboard(request):
	context = {}
	sessions = request.user.player.sessions_done[:5]
	context.update({'sessions': sessions})
	return render(request, template_name='dashboard.html', context=context)


@login_required
def training_summary(request):
	last_training = request.user.player.trainings.filter(is_done=True).order_by('-done_date').first()
	context = {'last_training': last_training}
	return render(request, template_name='training_summary.html', context=context)


@login_required
def trainings_chart(request):
	trainings_done = request.user.player.trainings.filter(is_done=True).order_by('-done_date')
	context = {'trainings_done': trainings_done}
	return render(request, template_name='chart.html', context=context)


@method_decorator(login_required, name='dispatch')
class GoalView(SuccessMessageMixin, FormView):
	template_name = 'apply_goal.html'
	form_class = GoalForm
	success_url = '/trainings/training_session/'
	success_message = 'Your goal was set'

	def get_form(self):
		return self.form_class(instance=self.request.user.player, **self.get_form_kwargs())

	def form_valid(self, form):
		goal = self.request.POST.get('goal')
		actual_record = self.request.POST.get('actual_record')
		if goal > actual_record * 2:
			self.success_message += ', but remember - it might be too hard to achieve with your current record'
		form.save()
		return super(GoalView, self).form_valid(form)


def process_session(session, request):
	''' Process session with passed data (mark as done etc)
	:param session: TrainingSession object
	:param request: request data
	:return: actualized session
	'''
	if 'reps_done' in request.POST and request.POST.get('reps_done'):
		reps_done = int(request.POST.get('reps_done'))
		# in case of cheating
		if reps_done > request.user.player.goal:
			reps_done = 0
		session.reps_done = reps_done
		session.completed = False
	else:
		session.reps_done = int(request.user.player.goal * session.ratio)
		session.completed = True
	session.is_done = True
	session.done_date = datetime.datetime.now()
	session.save()

	return session


@login_required
def training_session(request):
	context = {}
	session = request.user.player.get_training_session()
	if session is None:
		return GoalView.as_view()(request)
	if request.method == 'POST':
		process_session(session=session, request=request)
		return training_summary(request)
	context.update({'session': session})
	return render(request, template_name='training_session.html', context=context)


class PlayerView(SuccessMessageMixin, FormView):
	template_name = 'register_player.html'
	form_class = PlayerForm
	success_url = '/login'
	success_message = 'Your account was created'

	def form_valid(self, form):
		form.save()
		return super(PlayerView, self).form_valid(form)
